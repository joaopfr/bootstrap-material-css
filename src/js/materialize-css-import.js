import $ from 'jquery';

$(document).ready(function() {
    $('.tabs').tabs();
    $('.collapsible').collapsible();
    $('.carousel.carousel-slider').carousel({
        fullWidth: true,
        indicators: true
    });
});

import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css';
