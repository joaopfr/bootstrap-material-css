console.log('es6.js');
const greetings = (text, person) => {
    return `${text}, ${person}. Hello.`;
};
const s = new Set();
s.add("hello").add("bye");

const timeout = (duracao = 0) => {
    return new Promise((resolver, _) => {
        setTimeout(resolver, duracao);
    });
};

export default {
    bebidas: [
        'suco', 'refri'
    ],
    greetings: greetings,
    s: s,
    timeout: timeout
};